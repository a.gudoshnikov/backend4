<?php

header('Content-Type: text/html; charset=UTF-8');

//Создаём массив символов, в котором будем хранить наше сообщение
$result_message = array();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    //Если в массиве позиция save не является пустой, то удаляем cookie и записываем в сообщение результат
    if(!empty($_COOKIE['save'])){
        setcookie('save', '', 100000);
        $result_message[] = 'Спасибо, результаты сохранены и занесены в базу.';
    }

    //Создаём массив ошибок
    //Создаём элемент в массиве error
    //Если элемент непустой, то запишем в результирующий массив сообщение и удаляем cookie
    //Аналогично для каждого поля

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    if($errors['fio']){
        setcookie('fio_error', '', 100000);
        $result_message[] = '<div> Заполните имя.</div>';
    }

    $errors['email'] = !empty($_COOKIE['email_error']);
    if($errors['email']){
        setcookie('email_error', '', 100000);
        $result_message[] = '<div> Заполните email.</div>';
    }

    $errors['superpower'] = !empty($_COOKIE['superpower_error']);
    if($errors['superpower']){
        setcookie('superpower_error', '', 100000);
        $result_message[] = '<div> Вы не выбрали суперспособность.</div>';
    }

    $errors['date'] = !empty($_COOKIE['date_error']);
    if($errors['date']){
        setcookie('date_error', '', 100000);
        $result_message[] = '<div> Заполните дату рождения.</div>';
    }
    
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    if($errors['sex']){
        setcookie('sex_error', '', 100000);
        $result_message[] = '<div> Выберите ваш пол.</div>';
    }

    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    if($errors['limbs']){
        setcookie('limbs_error', '', 100000);
        $result_message[] = '<div> Выберите количество конечностей.</div>';
    }

    $errors['check'] = !empty($_COOKIE['check_error']);
    if($errors['check']){
        setcookie('check_error', '', 100000);
        $result_message[] = '<div> Вы должны принять условия контракта.</div>';
    }

    //Создаём массив значений наших полей 
    //Используя условные выражения проверим существование этих значений 
    //И в зависимости от этого либо запишем в каждый элемент values пустую строку, либо
    //перепишем значение из формы
    $values = array();
    $values['fio'] = empty($_COOKIE['value_of_fio']) ? '' : $_COOKIE['value_of_fio'];
    $values['email'] = empty($_COOKIE['value_of_email']) ? '' : $_COOKIE['value_of_email'];
    $values['date'] = empty($_COOKIE['value_of_date']) ? '' : $_COOKIE['value_of_date'];
    $values['sex'] = empty($_COOKIE['value_of_sex']) ? '' : $_COOKIE['value_of_sex'];
    $values['limbs'] = empty($_COOKIE['value_of_limbs']) ? '' : $_COOKIE['value_of_limbs'];
    $values['check'] = empty($_COOKIE['value_of_check']) ? '' : $_COOKIE['value_of_check'];
    $values['superpower'] = array();
    $values['superpower'][0] = empty($_COOKIE['superpower_0']) ? '' : $_COOKIE['superpower_0'];
    $values['superpower'][1] = empty($_COOKIE['superpower_1']) ? '' : $_COOKIE['superpower_1'];
    $values['superpower'][2] = empty($_COOKIE['superpower_2']) ? '' : $_COOKIE['superpower_2'];
    include('form.php');
}
else {
    $errors = false;
//Проверка на корректность заполнения полей

if(empty($_POST['fio'])){
    setcookie('fio_error', '1', time() + 24*60*60);
    $errors = true;
}
else{
    setcookie('value_of_fio', $_POST['fio'], time() + 30*24*60*60);
}

if(empty($_POST['superpower'])){
    setcookie('superpower_error','1', time() + 30*24*60*60);
    $errors = true;  
}
else{
    setcookie('value_of_superpower', $_POST['superpower'], time() + 30*24*60*60);
}

if(!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])){
    setcookie('email_error', '1', time() + 24*60*60);
    $errors = true;
}
else{
    setcookie('value_of_email', $_POST['email'], time() + 30*24*60*60);
}

if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
    setcookie('date_error', '1', time() + 24*60*60);
    $errors = true;
}else{
    setcookie('value_of_date', $_POST['date'], time() + 30*24*60*60);
}

if (!preg_match('/^[MF]$/', $_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_sex', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }

  if (!preg_match('/^[0-4]$/', $_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_limbs', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }

  if (!isset($_POST['check'])) {
    setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('value_of_check', $_POST['check'], time() + 30 * 24 * 60 * 60);
  }

  setcookie('superpower_0', '', 100000);
  setcookie('superpower_1', '', 100000);
  setcookie('superpower_2', '', 100000);

  foreach($_POST['superpower'] as $super) {
    setcookie('superpower_' . ($super - 1), 'true', time() + 30 * 24 * 60 * 60 * 12);
    }


    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else{
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('check_error', '', 100000);
    }

    setcookie('save', '1');


    $conn = new PDO("mysql:host=localhost;dbname=u41048", 'u41048', '1308338', array(PDO::ATTR_PERSISTENT => true));

    try{
    $user = $conn->prepare("INSERT INTO form SET fio = ?, email = ?, your_date = ?, sex = ?, limbs = ?, bio = ?, accept = ?");
    $user -> execute([$_POST['fio'], $_POST['email'], date('Y-m-d', strtotime($_POST['date'])), $_POST['sex'], $_POST['limbs'], $_POST['biography'], $_POST['check']]);
    $id_user = $conn->lastInsertId();
    $user1 = $conn->prepare("INSERT INTO superpowers SET id = ?, your_power = ?");
    foreach ($_POST['superpower'] as $sup)
        $user1 -> execute([$id_user, $sup]);
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}

setcookie('value_of_fio', '', 100000);
setcookie('value_of_email', '', 100000);
setcookie('value_of_sex', '', 100000);
setcookie('value_of_limbs', '', 100000);
setcookie('value_of_date', '', 100000);
setcookie('value_of_check', '', 100000);
setcookie('superpower_0', '', 100000);
setcookie('superpower_1', '', 100000);
setcookie('superpower_2', '', 100000);

header('Location: index.php');
}

?>
